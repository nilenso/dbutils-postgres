# dbutils-postgres (WIP)

* Note: This is still WIP, so don't use it just yet :) *

Building an util library around postgres with common abstractions that make it easy
to work with it.

This includes:
- Abstractions for common CRUD db operations.
- Coercions for working with different data types when writing to/reading from
  postgres.

## Why?

- Common code for any production clojure application.
- Use tested util functions with confidence.

## Installation

FIXME

## Usage

FIXME: explanation

    $ java -jar dbutils-postgres-0.1.0-standalone.jar [args]

## Examples

FIXME

## License

Copyright © 2018 Nilenso Software

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
