(ns dbutils-postgres.coercions-test
  (:require [dbutils-postgres.coercions :as sut]
            [clojure.test :refer :all]
            [clojure.java.jdbc :as jdbc]
            [dbutils-postgres.config :as test-config]))

(deftest test-clojure-type-coercions
  (testing "Should coerce clojure types to appropriate postgresql data types"
    (jdbc/with-db-connection [conn test-config/db-spec]
      (let [query1 (fn [& args] (first (jdbc/query conn (vec args))))]
        (is (= {:x [1 2 3 4]} (query1 "SELECT ?::int[] AS x" [1 2 3 4])))
        (is (= {:x {"foo" {"bar" 1}}} (query1 "SELECT ?::jsonb AS x" {"foo" {"bar" 1}})))
        (is (= {:x {"foo" {"bar" 1}}} (query1 "SELECT ?::jsonb AS x" {:foo {:bar 1}})))
        (is (= {:x {"foo" {"bar" 1}}} (query1 "SELECT ?::json AS x" {"foo" {"bar" 1}})))
        (is (= {:x {"foo" {"bar" 1}}} (query1 "SELECT ?::json AS x" {:foo {:bar 1}})))
        (is (= {:x ["a" "b" "c"]} (query1 "SELECT ?::text[] AS x" ["a" "b" "c"])))
        (is (= {:x ["a" "1" "B" "2.0"]} (query1 "SELECT ?::varchar[] AS x" ["a" 1 "B" 2.0])))))))

(deftest test-sql-type-coercions
  (testing "Should coerce postgresql data types to appropriate clojure types"
    (jdbc/with-db-connection [conn test-config/db-spec]
      (let [query1 (fn [& args] (first (jdbc/query conn (vec args))))]
        ;; (is (= {:x [1 2 3]} (query1 "SELECT '1 2 3'::oidvector AS x")))
        (is (= {:x ["a" "b"]} (query1 "SELECT '{a,b}'::text[] AS x")))
        (is (= {:x {"foo" 1}} (query1 "SELECT '{\"foo\":1}'::json AS x")))
        (is (= {:x {"foo" 1}} (query1 "SELECT '{\"foo\":1}'::jsonb AS x")))))))
