(ns dbutils-postgres.jdbc-test
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.test :refer :all]
            [dbutils-postgres.config :as test-config]
            [dbutils-postgres.helpers :as helpers]
            [dbutils-postgres.jdbc :as jdbc-utils]))

(defn create-test-table []
  (jdbc/db-do-commands
   test-config/db-spec
   (jdbc/create-table-ddl :person_test
                          [[:first_name "varchar(50) not null"]
                           [:last_name "varchar(50) not null"]
                           [:age :int "not null"]])))

(defn destroy-test-table []
  (jdbc/db-do-commands
   test-config/db-spec
   (jdbc/drop-table-ddl :person_test)))

(defn wrap-fixtures [f]
  (create-test-table)
  (f)
  (destroy-test-table))

(use-fixtures :each wrap-fixtures)

(deftest test-create
  (testing "Should create a database record successfully."
    (let [record {:first-name "jane" :last-name "doe" :age 30}]
      (jdbc/with-db-connection [conn test-config/db-spec]
        (is (= record
               (jdbc-utils/create! conn :person_test record)))))))

(deftest test-execute
  (testing "Should be able to execute an arbitrary query successfully."
    (jdbc/with-db-connection [conn test-config/db-spec]
      (let [query "INSERT INTO person_test(FIRST_NAME, LAST_NAME, AGE) VALUES ('jane', 'doe', 30);"
            res   (jdbc-utils/execute! conn query)]
        (is (= {:first-name "jane" :last-name "doe" :age 30}
               (-> (jdbc/query conn "SELECT * FROM person_test")
                   first
                   helpers/record->map)))))))

(deftest test-update
  (testing "Should be able to update an existing row in database."
    (jdbc/with-db-connection [conn test-config/db-spec]
      (let [record       {:first-name "jane" :last-name "doe" :age 30}
            _            (jdbc-utils/create! conn :person_test record)
            data         {:age 35}
            where-clause ["first_name = ?" (:first-name record)]
            res          (jdbc-utils/update! conn :person_test data where-clause)]
        (is (= (merge record data)
               (-> (jdbc/query conn "SELECT * FROM person_test")
                   first
                   helpers/record->map)))))))

(deftest test-upsert-new-row
  (testing "Upsert-ing a new row should insert it."
    (jdbc/with-db-connection [conn test-config/db-spec]
      (let [record       {:first-name "jane" :last-name "doe" :age 30}
            where-clause ["first_name = ?" (:first-name record)]
            res          (jdbc-utils/upsert! conn :person_test record where-clause)]
        (is (= record
               (-> (jdbc/query conn "SELECT * FROM person_test")
                   first
                   helpers/record->map)))))))

(deftest test-upsert-existing-row
  (testing "Upsert-ing an existing row should update it."
    (jdbc/with-db-connection [conn test-config/db-spec]
      (let [record       {:first-name "jane" :last-name "doe" :age 30}
            _            (jdbc-utils/create! conn :person_test record)
            data         (assoc record :age 45)
            where-clause ["first_name = ?" (:first-name record)]
            res          (jdbc-utils/upsert! conn :person_test data where-clause)]
        (is (= data
               (-> (jdbc/query conn "SELECT * FROM person_test")
                   first
                   helpers/record->map)))))))

(deftest test-delete
  (testing "Should delete an existing row."
    (jdbc/with-db-connection [conn test-config/db-spec]
      (let [record       {:first-name "jane" :last-name "doe" :age 30}
            _            (jdbc-utils/create! conn :person_test record)
            where-clause ["first_name = ?" (:first-name record)]
            res          (jdbc-utils/delete! conn :person_test where-clause)]
        (is (= nil (first (jdbc/query
                           conn
                           ["SELECT * FROM person_test where first_name = ?"
                            (:first-name record)]))))))))
