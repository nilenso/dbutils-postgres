(ns dbutils-postgres.helpers-test
  (:require [dbutils-postgres.helpers :as helpers]
            [clojure.test :refer :all]
            [cheshire.core :as json]))

(deftest test-parse-db-uri
  (testing "Should parse a db uri string successfully."
    (is (= (helpers/parse-db-url "postgresql://username:password@localhost:5432/kulu_backend_dev")
           {:classname "org.postgresql.Driver"
            :subprotocol "postgresql"
            :subname "//localhost:5432/kulu_backend_dev"
            :user "username"
            :password "password"}))))

(deftest test-pgobject-coercion
  (testing "Should convert a given type and value to a PGObject of same type and value."
    (let [value "dbutils-postgres"
          type  "string"
          pgobj (helpers/->pg-object  type value)]
      (is (= (.getType pgobj)  type))
      (is (= (.getValue pgobj) value)))))

(deftest test-json-pgobject-coercion
  (testing "Should convert a given map to a PGObject."
    (let [value {:name "dbutils-postgres" :version "0.1"}
          pgobj (helpers/clj->json-pg-object "json" (json/encode value))]
      (is (= (.getType pgobj) "json"))
      (is (= (json/decode (.getValue pgobj) (fn [k] (keyword k))) value)))))

(deftest test-pgobject-to-map-coercion
  (testing "should convert a PGObject of json/jsonb to a clojure map."
    (let [value  {"name" "dbutils-postgres" "version" "0.1"}
          pgobj  (helpers/clj->json-pg-object "json" (json/encode value))
          result (helpers/json-pg-object->clj pgobj)]
      (is (= value result)))))

(deftest test-keyword-to-column-name
  (testing "Should convert a clojure keyword to a database column name."
    (is (= "first_name" (helpers/keyword->colname :first-name)))
    (is (= "age" (helpers/keyword->colname :age)))))

(deftest test-column-name-to-keyword
  (testing "Should convert a database column name to clojure keyword."
    (is (= :first-name (helpers/colname->keyword "first_name")))
    (is (= :age (helpers/colname->keyword "age")))))

(deftest test-database-record-to-clojure-map
  (testing "Should keywordize column name keys in a database record."
    (is (= {:first-name "Jane" :last-name "Doe" :age 32}
           (helpers/record->map {"first_name" "Jane" "last_name" "Doe" "age" 32})))))
