(ns dbutils-postgres.coercions
  "Extends `IResultSetReadColumn` and `ISQLParameter` to coerce clojure
  structures into postgresql types and vice versa. Simply requiring
  this namespace enables all the coercions."
  (:require [clojure.java.jdbc :as jdbc]
            [dbutils-postgres.helpers :as helpers])
  (:import [clojure.lang IPersistentMap IPersistentVector]
           [java.sql PreparedStatement Timestamp]
           [java.time Instant ZoneId]
           [java.util Calendar TimeZone]
           [org.postgresql.jdbc PgArray]
           [org.postgresql.util PGobject]))

;; Extends `jdbc/IResultSetReadColumn` to provide
;; simple transformations of SQL values to Clojure values when
;; processing a ResultSet object

(extend-protocol jdbc/IResultSetReadColumn
  Timestamp
  ;; NOTE: Timestamp instance is retrieved as UTC
  (result-set-read-column [^Timestamp v _ _]
    (-> v (.toLocalDateTime) (.atZone (ZoneId/of "UTC")) (.toInstant)))

  PgArray
  (result-set-read-column [arr _ _]
    (vec (.getArray ^PgArray arr)))

  PGobject
  (result-set-read-column [pg-obj _ _]
    (case (.getType pg-obj)
      "jsonb" (helpers/json-pg-object->clj pg-obj)
      "json"  (helpers/json-pg-object->clj pg-obj)
      pg-obj)))

;; - Extends `jdbc/ISQLParameter` to provide a more sophisticated
;; transformation of Clojure values to SQL values that lets you
;; override how the value is stored in the `PreparedStatement`

(extend-protocol jdbc/ISQLParameter
  Instant
  ;; NOTE: Instant instance is stored as UTC
  (set-parameter [^Instant v ^PreparedStatement stmt ^long idx]
    (.setTimestamp stmt
                   idx
                   (Timestamp/from v)
                   (Calendar/getInstance (TimeZone/getTimeZone "UTC"))))

  IPersistentVector
  (set-parameter [v ^PreparedStatement stmt ^long i]
    (let [conn      (.getConnection stmt)
          meta      (.getParameterMetaData stmt)
          type-name (.getParameterTypeName meta i)]
      (if-let [elem-type (when (= (first type-name) \_) (apply str (rest type-name)))]
        (.setObject stmt i (.createArrayOf conn elem-type (to-array v)))
        (.setObject stmt i v))))

  IPersistentMap
  (set-parameter [^IPersistentMap v ^PreparedStatement stmt ^long idx]
    (.setObject stmt idx (helpers/clj->json-pg-object "json" v))))
