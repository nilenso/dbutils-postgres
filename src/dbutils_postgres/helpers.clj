(ns dbutils-postgres.helpers
  (:require [camel-snake-kebab.core :as csk]
            [cheshire.core :as json]
            [medley.core :refer [map-keys]])
  (:import [clojure.lang IPersistentMap]
           [org.postgresql.util PGobject]))

(defn parse-db-url
  "Extracts relevant information from a database url like
  postgresql://username:password@localhost:5432/kulu_backend_dev"
  [url]
  (let [[_ user password host port db]
        (re-find #"^.*://(.*):(.*)@(.*):(.*)/(.*)$" url)]
    {:classname "org.postgresql.Driver"
     :subprotocol "postgresql"
     :subname (str "//" host ":" port "/" db)
     :user user
     :password password}))

(defn ->pg-object [^String type ^String value]
  (doto (PGobject.)
    (.setType type)
    (.setValue value)))

(defn clj->json-pg-object [type x]
  "Converts a clojure map to a json PgObject depending on the type.

   :type can be either `jsonb` or `json`"
  (->pg-object type (if (instance? IPersistentMap x)
                      (json/encode x)
                      x)))

(defn json-pg-object->clj [^PGobject pg-obj]
  (json/decode (.getValue pg-obj)))

(defn keyword->colname [args]
  (csk/->snake_case_string args :separator \-))

(defn colname->keyword [args]
  (csk/->kebab-case-keyword args :separator \_))

(defn record->map [record]
  (map-keys #(-> % name colname->keyword) record))
