(ns dbutils-postgres.jdbc
  (:require [clojure.java.jdbc :as jdbc]
            [dbutils-postgres.helpers :as helpers])
  (:import [java.time Instant ZoneId]))

(def ^:private jdbc-options-map
  {:identifiers helpers/colname->keyword
   :entities    helpers/keyword->colname})

(defn create! [tx table row-map]
  (->> (jdbc/insert! tx table row-map jdbc-options-map)
       first
       helpers/record->map))

(defn update! [tx table row-map where-clause]
  (jdbc/update! tx table row-map where-clause jdbc-options-map)
  nil)

(defn upsert!
  "Updates columns or inserts a new row in the specified table"
  [tx table row-map where-clause]
  (let [result (jdbc/update! tx table
                             row-map
                             where-clause
                             jdbc-options-map)]
    (if (zero? (first result))
      (create! tx table row-map)
      result)))

(defn delete!
  "Deletes row(s) from table based on provided clause"
  [tx table where-clause]
  (jdbc/delete! tx
                table
                where-clause))

(defn execute!
  "Executes the query provided"
  ([tx query]
   (execute! tx query {:row-fn helpers/record->map
                       :transaction? false}))
  ([tx query opts]
   (let [defaults {:row-fn helpers/record->map :transaction? false}]
     (jdbc/execute! tx query (merge defaults opts)))))
